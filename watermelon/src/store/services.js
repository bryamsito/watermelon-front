import Axios from 'axios'
import albumService from '../services/AlbumService'
import msToHms from 'ms-to-hms'

// Axios Configuration
Axios.defaults.headers.common.Accept = 'application/json';
Axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';

export default {
  albumService: new albumService(Axios,msToHms)
}
