import Vue from 'vue'
import Router from 'vue-router'

import Default from '@/components/Default'
import Album from "../components/Album";
import AlbumDetail from "../components/AlbumDetail";

Vue.use(Router)

const routes = [
  { path: '/', name: 'Album', component: Album },
  { path: '/album/:id', name: 'AlbumDetail', component: AlbumDetail },
];

export default new Router({
  routes
})
