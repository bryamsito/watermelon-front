
class AlbumService {
  axios;
  baseUrl;
  msToHms;

  constructor(axios,msToHms) {
    this.axios = axios;
    this.baseUrl = 'http://localhost:8000/'
  }

  getAlbumsByName(name) {
    let self = this;
    self.axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
    self.axios.defaults.headers.common['Access-Control-Allow-Methods'] = '*';
    return self.axios.get(`${self.baseUrl}get-album/${name}`);
  }

  getInfoAlbum(id){
    let self = this;
    self.axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
    self.axios.defaults.headers.common['Access-Control-Allow-Methods'] = '*';
    return self.axios.get(`${self.baseUrl}get-info-album/${id}`);
  }

  getDurationTrack(track){
    let self = this;

  }
}

export default AlbumService
